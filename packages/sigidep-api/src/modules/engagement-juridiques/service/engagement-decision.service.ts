import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from '@entities/user.entity';
import { EngagementDecisionEntity } from '@entities/engagement-decision.entity';
import { EngagementDecisionDTO } from '../dto/create-engagement-decision.dto';
import { EtatEngagementEnum } from '@entities/engagement-juridique.entity';
import { ProcedureDecision } from '../types';
import { EngagementFilter } from '@utils/engagement-filter';

@Injectable()
export class EngagementDecisionService {
  constructor(
    @InjectRepository(EngagementDecisionEntity)
    private readonly repository: Repository<EngagementDecisionEntity>,
  ) {}

  public getRepository(): Repository<EngagementDecisionEntity> {
    return this.repository;
  }

  public async filter(
    filter?: EngagementFilter,
  ): Promise<EngagementDecisionEntity[]> {
    return this.repository
      .createQueryBuilder('ed')
      .leftJoinAndSelect('ed.taxesApplicable', 'taxe')
      .where(filter?.procedures ? 'ed.codeProcedure IN(:...codes)' : 'true', {
        codes: filter?.procedures,
      })
      .andWhere(filter?.etats ? 'ed.etat IN(:...etats)' : 'true', {
        etats: filter?.etats,
      })
      .andWhere(filter?.numeros ? 'ed.numero IN(:...numero)' : 'true', {
        numero: filter?.numeros,
      })
      .getMany();
  }

  public async deleteOne(id: number): Promise<any> {
    return this.repository.delete({ id });
  }

  public async create(
    payload: EngagementDecisionDTO,
    user: UserEntity,
  ): Promise<EngagementDecisionEntity> {
    payload.etat = EtatEngagementEnum.SAVE;
    const val1: string = payload.adminUnit?.substring(2, 4);
    const val2: string = (
      '00000' + Number(Math.floor(Math.random() * 100000))
    ).slice(-5);

    payload.numero = payload.exercise + 'CE' + val1 + '-' + val2;
    return this.repository.save({
      ...(payload as any),
      createdBy: user,
    });
  }

  public async update(
    payload: EngagementDecisionDTO,
    user: UserEntity,
    reserve: boolean = false,
  ): Promise<EngagementDecisionEntity> {
    const check = await this.repository.findOne({
      numero: payload.numero,
    });

    if (!check) {
      throw new NotFoundException();
    }
    payload = {
      ...(payload as any),
      etat: reserve ? EtatEngagementEnum.RESERVED : EtatEngagementEnum.MODIFY,
      montantAE_Reserve: reserve ? payload.montantAE : 0
    };
    return this.repository.save({
      ...(payload as any),
      updateBy: user,
    });
  }
}
