export const TableColumns = [
  {
    field: 'numero',
    title: 'tables.headers.numero',
    sortable: false,
  },
  {
    field: 'reference',
    title: 'tables.headers.reference',
    sortable: false,
  },
  {
    field: 'objet',
    title: 'tables.headers.objet',
    sortable: true,
  },
  {
    field: 'matriculeGestionnaire',
    title: 'tables.headers.beneficiaire',
    sortable: true,
  },
  {
    field: 'imputation',
    title: 'tables.headers.imputation',
    sortable: true,
  },
  {
    field: 'montantAE',
    title: 'tables.headers.montantAE',
    sortable: false,
  },
/*   {
    field: 'montantAE_Reserve',
    title: 'tables.headers.montantAE_Reserve',
    sortable: false,
  }, */
  {
    field: 'etat',
    title: 'tables.headers.etat',
    sortable: true,
  },



/*
  {
    field: 'exercise',
    title: 'tables.headers.exercise',
    sortable: true,
  },
  {
    field: 'subProgram',
    title: 'tables.headers.sousProgramme',
    sortable: true,
  },
  {
    field: 'action',
    title: 'tables.headers.action',
    sortable: true,
  },
  {
    field: 'activity',
    title: 'tables.headers.activity',
    sortable: true,
  },
  {
    field: 'task',
    title: 'tables.headers.task',
    sortable: true,
  },


  {
    field: 'adminUnit',
    title: 'tables.headers.adminUnit',
    sortable: false,
  },
 */


];
