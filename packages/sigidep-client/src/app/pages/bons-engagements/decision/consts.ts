export const TableColumns = [
  {
    field: 'numero',
    title: 'tables.headers.numeroBon',
    sortable: true,
  },
  {
    field: 'numActeJuridique',
    title: 'tables.headers.numeroJuridique',
    sortable: true,
  },
  {
    field: 'objet',
    title: 'tables.headers.objetBon',
    sortable: true,
  },
  {
    field: 'matriculeGestionnaire',
    title: 'tables.headers.beneficiaire',
    sortable: true,
  },
  {
    field: 'etat',
    title: 'tables.headers.etat',
    sortable: true,
  },
  {
    field: 'montantCPChiffres',
    title: 'tables.headers.montantCPChiffresBon',
    sortable: true,
  },
  {
    field: 'montantCPReserver',
    title: 'tables.headers.montantCPReserver',
    sortable: true,
  },
  {
    field: 'montantCPMandater',
    title: 'tables.headers.montantCPMandater',
    sortable: true,
  },
  {
    field: 'dateEngagement',
    title: 'tables.headers.dateEngagementBon',
    sortable: true,
  },


];
