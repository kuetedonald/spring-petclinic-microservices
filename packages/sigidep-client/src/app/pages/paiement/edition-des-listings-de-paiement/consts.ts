export const TableColumnsBordereau = [
  {
    field: 'numeroBon',
    title: 'tables.headers.numeroBon',
    sortable: true,
  },
  {
    field: 'numeroEng',
    title: 'tables.headers.numeroEng',
    sortable: true,
  },
  {
    field: 'numeroMandat',
    title: 'tables.headers.numeroMandat',
    sortable: true,
  },
  {
    field: 'matriculeGestionnaire',
    title: 'tables.headers.beneficiaire',
    sortable: true,
  },
  {
    field: 'objet',
    title: 'tables.headers.objetBon',
    sortable: true,
  },
  {
    field: 'imputation',
    title: 'tables.headers.imputation',
    sortable: true,
  },
  {
    field: 'montantMandate',
    title: 'tables.headers.montantMandate',
    sortable: true,
  },
  {
    field: 'dateMandatement',
    title: 'tables.headers.dateMandatement',
    sortable: true,
  },
  {
    field: 'etat',
    title: 'tables.headers.etat',
    sortable: true,
  }
];
