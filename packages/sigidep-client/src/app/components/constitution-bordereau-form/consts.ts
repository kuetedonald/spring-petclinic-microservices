export const TableColumnsBordereau = [

  {
    field: 'numeroBon',
    title: 'tables.headers.numeroBon',
    sortable: true,
  },
  {
    field: 'numeroEng',
    title: 'tables.headers.numeroEng',
    sortable: true,
  },
  {
    field: 'matriculeGestionnaire',
    title: 'tables.headers.beneficiaire',
    sortable: true,
  },
  {
    field: 'objet',
    title: 'tables.headers.objetBon',
    sortable: true,
  },
  {
    field: 'imputation',
    title: 'tables.headers.imputation',
    sortable: true,
  },
  {
    field: 'montantAE',
    title: 'tables.headers.montantAE',
    sortable: true,
  },
  {
    field: 'montantCP',
    title: 'tables.headers.montantCP',
    sortable: true,
  },
  {
    field: 'dateEng',
    title: 'tables.headers.dateEng',
    sortable: true,
  },
  {
    field: 'etat',
    title: 'tables.headers.etat',
    sortable: true,
  }
];
