export const articlesColumns = [
  {
    field: 'rubrique',
    title: 'tables.headers.rubrique',
    sortable: false,
  },
  {
    field: 'sousRubrique',
    title: 'tables.headers.sousRubrique',
    sortable: false,
  },
  {
    field: 'code',
    title: 'tables.headers.codeComplet',
    sortable: true,
  },
  {
    field: 'designation',
    title: 'tables.headers.designation',
    sortable: false,
  },
  {
    field: 'conditionnement',
    title: 'tables.headers.conditionnement',
    sortable: false,
  },
  {
    field: 'prix',
    title: 'tables.headers.prix',
    sortable: false,
  },
];
